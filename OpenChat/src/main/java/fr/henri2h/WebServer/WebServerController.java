package fr.henri2h.WebServer;

import org.json.JSONObject;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.henri2h.Shared.App;
import fr.henri2h.Shared.Objects.User;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by henri on 10/05/2017.
 */
@RestController
public class WebServerController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @CrossOrigin
    @RequestMapping("/Account/connection")
    public String connect(@RequestParam(value = "name") String name,
            @RequestParam(value = "password") String password) {
        return "NotImplemented";
    }

    @CrossOrigin
    @RequestMapping("/V2.0/Account/connection")
    public String v2_connect(@RequestHeader(value = "username") String username,
            @RequestHeader(value = "password") String password, @RequestHeader(value = "device_id") String clientID) {
        WebServer.server.AddDeviceID(username, clientID);
        return "Ok";
    }

    @CrossOrigin
    @RequestMapping("/Account/CreateUser")
    public String createUser(@RequestBody String body) throws Exception {

        JSONObject jObj = new JSONObject(body);
        JSONObject jObjReturn = new JSONObject();

        // should verify if token exist
        try {
            boolean result = WebServer.server.AddUser(
                    (new User(jObj.getString("username"), jObj.getString("name"), jObj.getString("password"))));
            jObjReturn.put("success", result);
        } catch (Exception e) {
            jObjReturn.put("success", false);
            App.logError("[ WebServerController ] : Cannot create users " + body, e);
        }

        return jObjReturn.toString();
    }

    @CrossOrigin
    @RequestMapping("/information")
    public String getInfo() {
        return "{Version:'" + WebServer.version + "'}";
    }

    @CrossOrigin
    @RequestMapping("isUp")
    public String getStatus() {
        return "OK";
    }
}
