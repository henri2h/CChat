package fr.henri2h.WebServer;

import fr.henri2h.Server.PushNotifactionHelper;

import fr.henri2h.Shared.App;
import fr.henri2h.Shared.FileManager;
import fr.henri2h.Shared.Objects.Conversation;
import fr.henri2h.Shared.Objects.ConversationListItem;
import fr.henri2h.Shared.Objects.ConversationMessage;
import fr.henri2h.Shared.Objects.DataType;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;

/**
 * Created by henri on 10/05/2017.
 */

@RestController
public class ConversationController {

    @CrossOrigin
    @RequestMapping(value = "/Conversation/ListConversations", produces = "application/json; charset=utf-8")
    public String ListConversations(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username) throws JSONException, IOException {

        List<ConversationListItem> conv = WebServer.server.ListConversations(username);

        // cast to JSON
        ObjectMapper mapper = new ObjectMapper();
        if (conv == null) {
            return "";
        } else {
            String toSend = mapper.writeValueAsString(conv);
            return toSend;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/v2/Conversation/ListConversations", produces = "application/json; charset=utf-8")
    public String v2_ListConversations(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username) throws JSONException, IOException {




        List<ConversationListItem> conv = WebServer.server.ListConversations(username);
      
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode listNode = objectMapper.valueToTree(conv);
        
        org.json.JSONArray convsJSONArray = new org.json.JSONArray(listNode.toString());

        // cast to JSON
        JSONObject jObjReturn = new JSONObject();
        if (conv == null) {
            jObjReturn.put("success", false);
            return jObjReturn.toString();
        } else {
            jObjReturn.put("success", true);
            jObjReturn.put("convs", convsJSONArray);
            return jObjReturn.toString();
        }
    }

    @CrossOrigin
    @RequestMapping("/Conversation/SetConversationName")
    public String setConversationName(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body)
            throws JSONException, IOException {

        App.log("Body : " + body);
        JSONObject obj = new JSONObject(body);
        String convID = obj.getString("ConvID");
        String convName = obj.getString("ConvName");

        if (WebServer.server.CanUserAccessConversation(username, token, convID)) {
            // change conversationName
            WebServer.server.SetConversationName(convID, convName);
            return "OK";
        } else {
            return "UNAUTORISED";
        }
    }

    // TODO : check if users actually exist
    @CrossOrigin
    @RequestMapping("/Conversation/AddConversation")
    public String addConversationName(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body)
            throws JSONException, IOException {
        JSONObject obj = new JSONObject(body);

        JSONArray arr = obj.getJSONArray("Users");
        String[] usernames = new String[arr.length()];
        for (int i = 0; i < arr.length(); i++) {
            usernames[i] = arr.getString(i);
        }

        String convName = obj.getString("ConvName");

        // createConversation
        boolean result = WebServer.server.CreateConversation(convName, usernames);
        if (result) {
            return "OK";
        } else {
            return "KO";
        }
    }

    @CrossOrigin
    @RequestMapping("/Conversation/DeleteConversation")
    public String deletConversation(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body)
            throws JSONException, IOException {

        JSONObject obj = new JSONObject(body);
        String convID = obj.getString("ConvID");

        if (WebServer.server.CanUserAccessConversation(username, token, convID)) {
            // createConversation
            boolean result = WebServer.server.DeleteConversation(convID);
            if (result) {
                return "{'success':true}";
            } else {
                return "{'success':false}";
            }
        } else {
            return "{'success':false,'reason':'unauthorized'}";
        }
    }

    @CrossOrigin
    @RequestMapping("/Conversation/CreateConversation")
    public void CreateConversation(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "x-username") String username, @RequestParam(value = "ConvName") String convName,
            @RequestBody String body) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String[] usernames = objectMapper.readValue(body, String[].class);

        WebServer.server.CreateConversation(convName, usernames);
    }

    @CrossOrigin
    @RequestMapping("/Conversation/SendConversationMessage")
    public String GetMessageFromUser(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body)
            throws IOException, JSONException {
        App.log("Body : " + body);
        // create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();

        ConversationMessage message = objectMapper.readValue(body, ConversationMessage.class);
        message.Username = username;

        if (WebServer.server.CanUserAccessConversation(username, token, message.ConvID)) {

            if (WebServer.server.AddConversationMessage(message)) {

                // push notifications
                App.log("Conv id : " + message.ConvID);

                // here : performance issue -> put usernames in conv and call only one function

                Conversation conversation = WebServer.server.GetConversation(message.ConvID);

                String title = username + "@" + WebServer.server.GetConversationName(message.ConvID);
                sendNotificationToUserInConv(message.ConvID, username, title, message.Content);

                return "OK";
            } else {
                return "KO";
            }
        } else {
            return "UNAUTORISED";
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/Conversation/GetConversation", produces = "application/json; charset=utf-8")
    public String SendConversationToUser(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body)
            throws IOException, JSONException {

        long start = new Date().getTime();
        JSONObject jObj = new JSONObject(body);
        String conversationID = jObj.getString("ConvID");
        if (conversationID.equals("")) {
            return "KO";
        } else if (WebServer.server.CanUserAccessConversation(username, token, conversationID)) {
            // get the conversation from the database
            Conversation conv = WebServer.server.GetConversation(conversationID);
            ObjectMapper mapper = new ObjectMapper();
            String conversation = mapper.writeValueAsString(conv);
            long end = new Date().getTime();
            long total = end - start;

            // byte[] conversationByte = conversation.getBytes("UTF-8");
            return conversation;
        } else {
            System.out.println(username + " wants to access a forbidden conversation : " + conversationID);
            App.log("[ SECURITY ] : " + username + " wants to access a forbidden conversation : " + conversationID);
            return "KO Forbidden";
        }
    }

    @CrossOrigin
    @RequestMapping("/Conversation/GetPicture")
    public byte[] SendPicture(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body)
            throws IOException, JSONException {

        JSONObject jObject = new JSONObject(body);
        String convID = jObject.getString("convID");
        String fileName = jObject.getString("fileName");

        if (WebServer.server.CanUserAccessConversation(username, token, convID)) {
            File file = FileManager.getFile(new String[] { "conversations", convID }, fileName);
            // System.out.println(file.toPath().toString());
            byte[] bytes = Files.readAllBytes(file.toPath());
            return bytes;

        } else {

            System.out.println("User : " + username + " is not allowed to access " + convID);
            return null;
        }
    }

    @CrossOrigin
    @RequestMapping("/Conversation/SendPicture")
    public String RecievePicture(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestHeader(value = "ConvID") String convID,
            @RequestBody byte[] content) throws Exception {

        if (WebServer.server.CanUserAccessConversation(username, token, convID)) {

            File file = File.createTempFile("img", "",
                    FileManager.getPath(new String[] { "conversations", convID }, true).toFile());// get path and create
                                                                                                  // directory if needed
            Files.write(file.toPath(), content);

            // create the message
            ConversationMessage cm = new ConversationMessage();
            cm.Username = username;
            cm.ConvID = convID;
            cm.DataType = DataType.Image;
            cm.Content = file.getName();
            WebServer.server.AddConversationMessage(cm);

            // send a push notification
            String title = username + "@" + WebServer.server.GetConversationName(convID);
            sendNotificationToUserInConv(convID, username, title, "Sent a picture");

            return "OK";
        } else {
            return "KO";
        }
    }

    @CrossOrigin
    @RequestMapping("/Conversation/ListUsers")
    public String ListUsers(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body) throws Exception {

        JSONObject jObj = new JSONObject(body);
        JSONObject jObjReturn = new JSONObject();

        try {
            List<String> usernames = WebServer.server.ListUsers();
            jObjReturn.put("usernames", usernames);
            jObjReturn.put("success", true);
        } catch (Exception e) {
            jObjReturn.put("success", true);
            App.logError("[ ConversationController ] : Cannot list users", e);
        }

        return jObjReturn.toString();
    }

    @CrossOrigin
    @RequestMapping("/Conversation/SearchForUser")
    public String SearchForUser(@RequestHeader(value = "token") String token,
            @RequestHeader(value = "username") String username, @RequestBody String body) throws Exception {

        JSONObject jObj = new JSONObject(body);
        JSONObject jObjReturn = new JSONObject();

        // should verify if token exist
        try {
            List<String> usernames = WebServer.server.SearchForUser(username, jObj.optString("search"));
            jObjReturn.put("usernames", usernames);
            jObjReturn.put("success", true);
        } catch (Exception e) {
            jObjReturn.put("success", false);
            App.logError("[ ConversationController ] : Cannot list users", e);
        }

        return jObjReturn.toString();
    }

    void sendNotificationToUserInConv(String convID, String username, String title, String content) {
        List<String> usernames = WebServer.server.GetUsersInConversation(convID);
        for (String user : usernames) {
            System.out.println("Going to send to : " + user);
            if (username.equals(user) == false) {
                System.out.println("Ok");
                List<String> devices_id = WebServer.server.GetDeviceID(user);

                // send the push notification
                PushNotifactionHelper.sendPushNotificationToDevices(devices_id, title, content);
            }
        }
    }
}
