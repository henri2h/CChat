package fr.henri2h.WebServer;

import fr.henri2h.Server.Server;
import fr.henri2h.Shared.App;
import fr.henri2h.Shared.Objects.User;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by henri on 10/05/2017.
 */

@SpringBootApplication
public class WebServer {

    public static String version = "BETA_1.36";

    public static Server server;

    public static void main(String[] args) {

        System.out.println("########################################");
        System.out.println("# Start web server version : " + version + " #");
        System.out.println("########################################");
        System.out.println("");

        App.createNewLog();

        // Working directory :
        System.out.println("Working Directory = " + System.getProperty("user.dir"));

        // Create the server
        server = new Server();

        ApplicationContext context = SpringApplication.run(WebServer.class, args);

        try { // launching console interpretation
            interprete();

            // then quit
            int exitValue = SpringApplication.exit(context);
            System.exit(exitValue);
        } catch (Exception ex) {
            System.out.println("Could not read the console output");
            // should keep the program running for running as a service
        }

    }

    public static void printHelp() {
        String[] linesToPrint = new String[] { "Help :", "In order to add a user : ",
                "   - add user <username> <name> <password>", "", "In order to add a conversation : ",
                "   - add conversation <userA userB ... userEnd>", "", "Set firebase id", "   - set firebase <id>",
                "Stop the server :", "   - stop" };

        for (String sIn : linesToPrint) {
            System.out.println(sIn);
        }
    }

    public static void interprete() {

        Scanner scan = new Scanner(System.in);
        String input = "";

        while (!input.equals("stop")) { // exit when the stop command has been given
            System.out.print("Command > ");
            input = scan.nextLine();
            String[] commandParts = input.split(" ");

            if (input.equals("help")) {
                printHelp();
            } else if (input.equals("stop") || input.equals("exit")) {
                App.logProgram("Stopping the server");
                break;
            } else if (commandParts.length >= 3) {
                if (commandParts[0].equals("add") && commandParts[1].equals("user")) {
                    // add user

                    String username = commandParts[2];
                    String name = commandParts[3];
                    String password = commandParts[4];

                    boolean result = server.AddUser((new User(username, name, password)));
                    if (result)
                        System.out.println("User : " + username + " added");
                } else if (commandParts[0].equals("add") && commandParts[1].equals("conversation")) {
                    // add conversation

                    List<String> users = new ArrayList<>();

                    for (int i = 2; i < commandParts.length; i++) {
                        users.add(commandParts[i]);
                    }

                    boolean result = server.CreateConversation("conversation", users.toArray(new String[users.size()]));
                    if (result) {
                        System.out.println("Success");
                    } else {
                        System.out.println("Failed");
                    }
                } else if (commandParts[0].equals("set") && commandParts[1].equals("firebase")) {
                    // set firebase id
                    String id = commandParts[2];
                    boolean result = server.setFirebaseID(id);
                } else {
                    System.out.println("Not a good arguments");
                }
            } else {
                System.out.println("Not a command");
            }
        }
    }
}
