package fr.henri2h.Client;

import javax.swing.*;
import java.awt.*;

/**
 * to remove
 */
@SuppressWarnings("serial")
class MessageText extends javax.swing.JPanel {
	JTextField textField = new JTextField(40);

    MessageText(String date, String userName, String message) {
        JLabel date1 = new JLabel();
		date1.setText(date);
		date1.setForeground(Color.blue);

        JLabel userName1 = new JLabel();
		userName1.setText(userName);

        JLabel message1 = new JLabel();
		message1.setText(message);

		this.add(date1);
		this.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
		this.add(userName1);
		this.add(new JSeparator(JSeparator.VERTICAL), BorderLayout.LINE_START);
		this.add(message1);

	}
}
