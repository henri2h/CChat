package fr.henri2h.Client;

import javax.swing.*;
import java.awt.*;

import static javax.swing.JOptionPane.showInputDialog;

/**
 * Created by Henri on 22-Jul-16.
 *
 * @author Henri
 * @author Gabe
 */

//TODO: call a function in the TCPClient class when the user press enter

class ClientWindow extends JFrame {

    private JPanel mainPanel;
    private JPanel panel;
    private JTextArea textbox;

    ClientWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(this.mainPanel);
        pack();
        setVisible(true);

        JLabel message = new JLabel();
        message.setText("Starting ...");
        message.setForeground(Color.red);

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(message);

        pack();
    }

    public String getConnectionInput() {

        /*ConnectionDialog userInput = new ConnectionDialog();
        while(userInput.isActive()){
            System.out.print(".");
        }

        System.out.println("ip : " + userInput.ip);
            return userInput.ip;*/
        return showInputDialog(this, "IP : ", JOptionPane.PLAIN_MESSAGE);
    }

    // don't really know how to name this function, if you have any idea ...
    public Username getUserClient() {
        /*UsernameDialog userInput = new UsernameDialog();
        while(userInput.isActive()){
            System.out.print(".");
        }
        return userInput.client;*/

        Username client = new Username();
        client.username = showInputDialog(this, "Username : ", JOptionPane.PLAIN_MESSAGE);
        client.password = showInputDialog(this, "Password : ", JOptionPane.PLAIN_MESSAGE);
        return client;
    }

    public void addLine(String Date, String UserName, String Message) {
        MessageText line = new MessageText(Date, UserName, Message);
        panel.add(line);

    }
    public void addMessage(String Message) {
        JLabel message = new JLabel();
        message.setText(Message);
        message.setForeground(Color.red);

        panel.add(message, BorderLayout.NORTH);

        this.pack();
    }
}
