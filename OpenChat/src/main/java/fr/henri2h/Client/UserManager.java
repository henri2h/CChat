package fr.henri2h.Client;

import fr.henri2h.Shared.User;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class UserManager {
	private final String main = "usersClient\\";
	private final Path Main;

	UserManager() {
		Main = Paths.get(main);
		loadUsers();
	}

	private void loadUsers() {
		try {
			Path myFile = Paths.get(main + "users.info");

			if (Files.exists(Main)) {

				User user = new User();

				List<String> content = Files.readAllLines(myFile, Charset.defaultCharset());

				for (String username : content) {
					Path info = Paths.get(main + username + "\\information");

					Object[] userInfo = Files.readAllLines(info, Charset.defaultCharset()).toArray();

					user.username = username;
					user.FullName = userInfo[0].toString();
					user.imagePath = main + username + "\\" + userInfo[1].toString();
					user.version = userInfo[2].toString();

					System.out.println("Loaded user " + user.username + " wiht full name : " + user.FullName + " "
							+ user.imagePath + " version : " + user.version);
				}

			} else {
				// nothing exist
				// we have to create the directory and populate it
				Files.createDirectory(Main);
				Files.createFile(myFile);
				ArrayList<User> users = TCPClient.getUsers();

				File file = myFile.toFile();
				PrintWriter out = new PrintWriter(new FileWriter(file));

				for (User user : users) {
					out.println(user.username);

					Path dirUser = Paths.get(main + user.username);
					Files.createDirectories(dirUser);

					File fileUser = new File(main + user.username + "//information");
					PrintWriter outUser = new PrintWriter(new FileWriter(fileUser));

					outUser.println(user.FullName);
					outUser.println(user.version);
					outUser.println(user.imagePath);

					outUser.close();
				}
				out.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}

	}
}
