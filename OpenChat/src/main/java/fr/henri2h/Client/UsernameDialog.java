package fr.henri2h.Client;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

/**
 * Created by Henri on 22-Jul-16.
 *
 * @author Henri
 * @author Gabe
 */

class UsernameDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tbUsername;
    private JPasswordField tbPassword;

    private Username client;

    private UsernameDialog() {
        setVisible(true);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE

        contentPane.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (KeyEvent.VK_ESCAPE == e.getKeyCode())
                {
                    onCancel();
                }
            }
        });

        pack();
    }

    private void onOK() {
        // add your code here

        client.username = tbUsername.getText();
        client.password = Arrays.toString(tbPassword.getPassword());
        System.out.println("fr.henri2h.Client password : " + client.password);
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        UsernameDialog dialog = new UsernameDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
