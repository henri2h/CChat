package fr.henri2h.Client;

import java.io.BufferedReader;
import java.io.IOException;


/**
 * Created by henri on 21/10/2016.
 */
class ServerMessageAnalizer extends Thread {

    private final ClientWindow window;
    private final BufferedReader inFromServer;

    public ServerMessageAnalizer(ClientWindow inputWindow, BufferedReader FromServer) {
        window = inputWindow;
        inFromServer = FromServer;
    }

    public void run() {
        // Create a client
        // // TODO: 28/11/16 add a way to close this loop
        boolean iterating = true;

        while (iterating) {

            String serverResponse;
            try {
                String methodMessage = inFromServer.readLine();

                if (methodMessage.equals("@message")) {
                    String userSend = inFromServer.readLine();
                    String Date = inFromServer.readLine();
                    serverResponse = inFromServer.readLine();
                    window.addLine(Date, userSend, serverResponse);
                } else {
                    serverResponse = inFromServer.readLine();
                    System.out.println("Un handled response : " + serverResponse);
                    window.addMessage(serverResponse);
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println("Error on reading server input");
            }
        }
    }
}
