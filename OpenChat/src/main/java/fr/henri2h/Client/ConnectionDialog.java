package fr.henri2h.Client;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Henri on 22-Jul-16.
 *
 * @author Henri
 * @author Gabe
 */

class ConnectionDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textbox;

    private ConnectionDialog() {
        setVisible(true);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        buttonCancel.addActionListener(e -> onCancel());
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        pack();
        buttonOK.addActionListener(e -> onOK());
        contentPane.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (KeyEvent.VK_ESCAPE == e.getKeyCode())
                {
                    onCancel();
                }
            }
        });
    }

    private void onOK() {
            // add your code here
        String ip = textbox.getText();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();

    }

    public static void main(String[] args) {
        ConnectionDialog dialog = new ConnectionDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
