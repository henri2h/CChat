package fr.henri2h.Client;

import fr.henri2h.Shared.Conversion;
import fr.henri2h.Shared.User;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Henri on 22-Jul-16.
 *
 * @author Henri
 * @author Gabe
 */
class TCPClient {
    private static final ClientWindow window = new ClientWindow();

    private static DataOutputStream outToServer;
    private static DataInputStream dataFromServer;
    private static BufferedReader inFromServer;

    public static void main(String argv[]) throws Exception {

        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        String ip = window.getConnectionInput();

        // if ip is void
        //// TODO: 22-Jul-16 : add a list box with all the ip previously used and display them only if there is a server running
        if (ip.equals("")) {
            //add a default address
            ip = "localhost";
        }

        // start the client
        Socket clientSocket = new Socket(ip, 6789);

        outToServer = new DataOutputStream(clientSocket.getOutputStream());

        InputStreamReader inputReader = new InputStreamReader(clientSocket.getInputStream());
        inFromServer = new BufferedReader(inputReader);
        dataFromServer = new DataInputStream(clientSocket.getInputStream());

        // starting the contact
        outToServer.writeBytes("#connection" + "\n");

        String authorisation = inFromServer.readLine();
        System.out.println("authorisation : " + authorisation);

        if (authorisation.equals("#ok")) {

            // checking the connection method
            String connectionMethod = inFromServer.readLine();

            Username client = window.getUserClient();

            String username = client.username;
            String password = client.password;

            if (connectionMethod.equals("#user")) {

                outToServer.writeBytes(username + "\n");
                outToServer.writeBytes(password + "\n");

            } else if (connectionMethod.equals("#plain")) {

                outToServer.writeBytes(username + "\n");
            }

            String correct = inFromServer.readLine();
            System.out.println("Connection : " + correct);

            // create a new thread to read the server message
            // TODO: create a better way to check the incomming messages

// create the tread annalizer
            ServerMessageAnalizer t = new ServerMessageAnalizer(window, inFromServer);
            t.start();

            // downloading the user list
            ArrayList<User> userRecieved = getUsers();
            if (userRecieved != null)
            for (User test : userRecieved) {
                System.out.println("Recieved : " + test.username);
            }

            // starting to read the message
            // what's going on with this ????
            // // TODO: 28/11/16 clean this up
            boolean iterrate = true;
            while (iterrate) {
                outToServer.writeBytes(inFromUser.readLine() + "\n");
            }
        } else if (authorisation.equals("#ok")) {
            System.out.println("Connection refused");
        }
        clientSocket.close();
    }

    public static void sendText(String message) {
        try {
            outToServer.writeBytes("#message" + "\n");
            outToServer.writeBytes(message + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<User> getUsers() {
        try {
            outToServer.writeBytes("#getUsers" + "\n");
            String response = inFromServer.readLine();

            if (response.equals("#okUsers")) {
                int size = dataFromServer.read();
                byte[] file = new byte[size];

                for (int i = 0; i < size; i++)
                    file[i] = dataFromServer.readByte();
                return Conversion.toUser(file);

            } else {
                System.out.println("Error in getting ArrayList : " + response);
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error getting users");
            return null;
        }

    }

    public static User getUser(int numb) {
        //// TODO: 22-Jul-16  : implement a function to return a specific user determined by numb
        return null;
    }
}

