package fr.henri2h.Server.Servers.TCPServer;

import fr.henri2h.Shared.App;

/**
 * Created by henri on 21/10/2016.
 */

class ListenerManager extends Thread {

    public void run() {
        try {
            new TCPListener();
        } catch (Exception e) {
            App.logError("fr.henri2h.Client disconected", e);
        }
    }
}
