package fr.henri2h.Server.Servers.TCPServer;

abstract class Observer {
	public abstract void update();
	public abstract void stop();
}
