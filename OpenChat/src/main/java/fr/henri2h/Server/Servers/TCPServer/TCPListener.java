package fr.henri2h.Server.Servers.TCPServer;

import fr.henri2h.Server.Objects.Usernames;
import fr.henri2h.Server.ServerUser.ServerUser;
import fr.henri2h.Server.ServerUser.ServerUsers;
import fr.henri2h.Server.ServerUser.userConverter;
import fr.henri2h.Server.conversationsTools.conversationManager;
import fr.henri2h.Shared.*;
import fr.henri2h.Shared.TCPConversation.TCPConversationElement;
import fr.henri2h.Shared.TCPConversation.TCPConversationMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Henri on 22-Jul-16.
 *
 * @author Gabe
 * @author Henri2h
 */

public class TCPListener extends Observer {

    private final connectionMethods connectionMethod = connectionMethods.anonymous;
    private int numb;
    private boolean isUserLoggedIn = false;
    private int messageUpdate = 0;

    private TCPServerSocketClient serverClient;

    //user information
    private Usernames user;
    //   private User clientUser;

    private ServerUser serverUser;
    private StringStream toUpdate;

    public TCPListener() {
        try {
            serverClient = new TCPServerSocketClient();

            toUpdate = new StringStream();
            numb = TCPServer.userNumb++;
            user = new Usernames();

            App.logProgram("Created a new instance, number : " + numb);

            serverClient.connect();
            App.logProgram("fr.henri2h.Client connected on : " + numb);

            // now, it is connected
            boolean connected = true;
            TCPServer.createNewThread(TCPServer.userNumb);


            serverClient.writeLine("@OK");

            while (connected) {
                String request = serverClient.readCommand();
                annaliseCommand(request);
            }
            App.logProgram("fr.henri2h.Client connected on " + numb + " is disconnected");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            App.logError("Error in TCPListener constructor for client : " + numb, e);
        }

    }

    private void annaliseCommand(String command) throws Exception {

        switch (command) {
            // connection
            case ("connection"):
                serverClient.writeCommand("connectionOK");
                break;
            case ("connectionMethod"):
                //for the moment we select the collection method
                //TODO: make a selection method in a config file
                serverClient.writeCommand(connectionMethod.toString());
                break;

            case ("connect"):
                serverClient.writeCommand("connectOK");
                connect();
                break;

            // connection status
            case ("isLoggedIn"):
                if (isUserLoggedIn) {
                    serverClient.writeCommand("connectedOK");
                } else {
                    serverClient.writeCommand("connectedKO");
                }
                break;


            default:
                if (isUserLoggedIn) {
                    switch (command) {
                        case ("users"):
                            serverClient.writeCommand("OK");
                            user();
                            break;
                        case ("toRead"):
                            serverClient.writeLine(Integer.toString(messageUpdate));
                            break;
                        case ("readMessageFromServer"):
                            String num = serverClient.readLine();

                            int mToRead = Integer.parseInt(num);
                            sendMessageFromServer(mToRead);
                            break;

                        // send the conversation to the client for the moment, it is only the name
                        case ("listConversations"):
                            serverClient.writeCommand("OK");
                            ObjectMapper mapper = new ObjectMapper();
                            String[] conv = serverUser.getConversation();

                            if (conv == null) {
                                serverClient.writeLine("");
                            } else {
                                String toSend = mapper.writeValueAsString(conv);
                                serverClient.writeLine(toSend);
                            }
                            break;

                        case ("sendConversationName"):
                            serverClient.writeCommand("OK");
                            String convID = serverClient.readLine();
                            String convName = conversationManager.getConversationName(convID);
                            serverClient.writeLine(convName);
                            break;
// TODO : rename the actions to mean realy what they are suposed to do : send to send to client add for example addMessageToConversation
                        // instead of sendMessageToConversation
                        case ("sendMessageToConversation"):
                            serverClient.writeCommand("OK");
                            readConversationMessage();
                            break;
                        case ("createConversation"):
                            serverClient.writeCommand("OK");
                            createConversation();
                            break;
                        case ("sendConversation"):
                            serverClient.writeCommand("OK");
                            sendConversation();
                            break;
                        // may not be useful
                        case ("sendPictureToConversation"):
                            serverClient.writeCommand("OK");
                            recievePicture();
                            break;
                        case ("getPictureFromConversation"):
                            serverClient.writeCommand("OK");
                            sendPicture();
                            break;

                        default:
                            serverClient.writeCommand("KO_Unknown");
                            // TODO : make a security log because this could mean that someone is trying to connect using an other client (possibly for doing bad things)
                            App.logError("Didn't know the command : " + command);
                            break;
                    }
                }
                break;
            // end of the switch
        }
    }

    // ============ connection ===========
    private void connect() throws IOException {
        // save the settings
        user.ip = serverClient.getIP();
        user.name = serverClient.readLine();
        user.password = serverClient.readLine();


        //get the user
        serverUser = ServerUsers.getServerUser(user.name);
        if (serverUser != null) {
            isUserLoggedIn = !connectionMethod.equals(connectionMethods.password) || user.password.equals(serverUser.getPassword());

            TCPServer.attach(this);
            serverClient.writeCommand("connectSuccess");
        } else {
            App.logError("Connection error for username : " + user.name + " in thread " + numb);
            serverClient.writeCommand("connectFailed");
        }
    }

    // conversations
    private void readConversationMessage() throws IOException {
        // // TODO: 28/11/16 implement this method, 80% done, now, we have to test
        String sMessage = serverClient.readLine();

        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        TCPConversationMessage message = objectMapper.readValue(sMessage, TCPConversationMessage.class);

        conversationManager.appendTCPMessage(message);
    }

    public void createConversation() throws IOException {
        String users = serverClient.readLine();
        ObjectMapper objectMapper = new ObjectMapper();

        String[] usersArray = objectMapper.readValue(users, String[].class);
        List<String> usersList = new ArrayList<>();
        for (String user : usersArray) {
            usersList.add(user);
        }

        conversationManager.createConversation(usersList, "conversations");
    }

    public void sendConversation() throws IOException {
        long start = new Date().getTime();

        String conversationID = serverClient.readLine();
        TCPConversationElement tcpConversation = conversationManager.getTCPConversation(conversationID);

        ObjectMapper mapper = new ObjectMapper();
        String conversation = mapper.writeValueAsString(tcpConversation);

        long end = new Date().getTime();
        long total = end - start;

        App.log("[fr.henri2h.Server : sendConversation] Duration : " + total);

        byte[] conversationByte = conversation.getBytes("UTF-8");
        serverClient.writeDataWithLenght(conversationByte);
    }

    public void sendPicture() throws IOException {
        String convID = serverClient.readLine();
        String fileName = serverClient.readLine();

        if (conversationManager.canAccess(user.name, convID)) {
            serverClient.writeCommand("OK");

            File file = FileManager.getFile(new String[]{"conversations", convID}, fileName);
            byte[] content = Files.readAllBytes(file.toPath());

            serverClient.writeDataWithLenght(content);
        } else {
            serverClient.writeCommand("KO");
        }
    }

    public void recievePicture() throws Exception {
        String convID = serverClient.readLine();
        String fileName = serverClient.readLine();

        if (conversationManager.canAccess(user.name, convID)) {
            serverClient.writeCommand("OK");
            File file = FileManager.getFile(new String[]{"conversations", convID}, fileName);
            byte[] content = serverClient.readDataWithLenght();
            Files.write(file.toPath(), content);
        } else {
            serverClient.writeCommand("KO");
        }
    }


    // ============== managing user    ================
    private void user() throws IOException {
        // we are in user mode
        String userRequest = serverClient.readCommand();
        if (userRequest.equals("getUser")) {

            String userRequested = serverClient.readLine();
            User user = ServerUsers.getUser(userRequested);
            if (user != null) {
                serverClient.writeCommand("notImplemented");
            }
        } else if (userRequest.equals("getUserImage")) {
            String userRequested = serverClient.readLine();
            byte[] image = ServerUsers.getImageUser(userRequested);
            if (image != null) {
                serverClient.writeDataWithLenght(image);
            } else {
                serverClient.writeCommand("KO");
            }
        }
    }


    public void sendUsers() {
        // // TODO: 28/11/16 maybe use this method or remove it
        try {
            ArrayList<ServerUser> inputUser = ServerUsers.serverUsers;
            if (inputUser != null) {
                System.out.println("Size before conversion :" + inputUser.size());
                ArrayList<User> decodedUser = userConverter.serverUsersToUsers(ServerUsers.serverUsers);
                System.out.println("Size after conversion :" + decodedUser.size());
                byte[] bytes = Conversion.toByte(decodedUser);

                serverClient.writeDataWithLenght(bytes);
            }
        } catch (IOException e) {
            App.logError("Error in sending users", e);
        }

    }


    @Override
    public void update() {

    }

    private void sendMessageFromServer(int num) throws IOException {
        while (num != 0) {
            String up = toUpdate.readLine();
            System.out.println("Message : " + num + " : " + up);
            serverClient.writeLine(up);
            num--;
            messageUpdate--;
        }
        System.out.print("\n");

    }


    @Override
    public void stop() {
        try {
            serverClient.stop();
        } catch (IOException e) {
            App.logError("Could not close the socket for " + numb, e);
        }
    }

}
