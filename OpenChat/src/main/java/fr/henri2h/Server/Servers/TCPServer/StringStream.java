package fr.henri2h.Server.Servers.TCPServer;

import java.io.*;
import java.nio.charset.Charset;

/**
 * Created by henri on 23/10/2016.
 */
class StringStream {

    private BufferedReader toRead;
    private final OutputStream toWrite;

    public StringStream() throws IOException {
        PipedOutputStream output = new PipedOutputStream();
        toWrite = output;
        InputStream input = new PipedInputStream(output);

        InputStreamReader readOutput = new InputStreamReader(input);
        toRead = new BufferedReader(readOutput);
    }

    public String readLine() throws IOException {
        return toRead.readLine();
    }

    public void writeLine(String text) throws IOException {
        text += "\n";
        byte[] towrite = text.getBytes(Charset.defaultCharset());
        toWrite.write(towrite);

    }
}
