package fr.henri2h.Server.Servers.TCPServer;

import fr.henri2h.Server.ServerConsole;
import fr.henri2h.Server.ServerUser.ServerUsers;
import fr.henri2h.Server.conversationsTools.conversationManager;
import fr.henri2h.Shared.App;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Henri on 22-Jul-16.
 *
 * @author Gabe
 * @author Henri2h
 */

public class TCPServer {


    private static final int serverPort = 6019;
    // =================== observer ====================
    /// update all the observers
    private static final List<Observer> observers = new ArrayList<>();
    public static int userNumb = 0;
    public static ServerSocket welcomeSocket;

    public static void main(String[] args) {
        App.createNewLog();

        ServerUsers.loadUsers();
        ServerUsers.getUsers();
        conversationManager.listConversations();

// creating the server socket
        createSocket();
        createNewThread(userNumb);

        // launching console interpretation
        ServerConsole.interprete();
    }

    public static void createSocket() {
        // create the socket
        try {

            App.logProgram("Creating server on : " + serverPort);
            welcomeSocket = new ServerSocket(serverPort);
        } catch (IOException e) {
            App.logError("Could not create the ServerSocket", e);
        }
    }

    public static void createNewThread(int userNum) {
        // this call a new thread to control with a client
        ListenerManager t = new ListenerManager();
        t.start();
    }

    public static void stopServer() {
        App.logProgram("Closing the server");

        observers.forEach(Observer::stop);
        try {
            welcomeSocket.close();
        } catch (IOException e) {
            App.logError("Could not close the server", e);
        }
    }

    private static void notifyAllObservers() {
        observers.forEach(Observer::update);
    }

    public static void attach(Observer observer) {
        observers.add(observer);
    }

}
