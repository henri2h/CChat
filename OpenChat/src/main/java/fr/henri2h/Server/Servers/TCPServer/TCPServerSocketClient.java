package fr.henri2h.Server.Servers.TCPServer;

import fr.henri2h.Shared.App;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by Henri on 02-Feb-17.
 */
public class TCPServerSocketClient {
    // iostream
    private DataInputStream inDataFromClient;

    private BufferedReader inFromClient;
    private DataOutputStream outToClient;

    private Socket connectionSocket;

    public void connect() throws IOException {
        connectionSocket = TCPServer.welcomeSocket.accept();
        //Create streams
        // input and output
        inDataFromClient = new DataInputStream(connectionSocket.getInputStream());
        inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
        outToClient = new DataOutputStream(connectionSocket.getOutputStream());
    }

    public String getIP() {
        return connectionSocket.getInetAddress().toString();
    }

    void sendFile(File Path) throws IOException {
        byte[] content = Files.readAllBytes(Path.toPath());
        writeDataWithLenght(content);
    }

    // recieve a file and erase the file with the same name
    void recieveFile(Path Path, boolean deletePreviousFile) throws IOException {
        int length = Integer.parseInt(inFromClient.readLine());
        byte[] buf = new byte[length];
        for (int read = 0; read < length; ) {
            read += inDataFromClient.read(buf, read, buf.length - read);
        }

    }

    public void writeLine(String text) throws IOException {
        write(text + System.lineSeparator());
    }

    public void write(String text) throws IOException {
        outToClient.writeBytes(text);
        outToClient.flush();

        App.logData(text, "traffic.log", "write " + connectionSocket.getRemoteSocketAddress(), false);
    }

    public void writeCommand(String text) throws IOException {
        writeLine("@" + text);
    }

    public String readLine() throws IOException {
     String input = inFromClient.readLine();
        App.logData(input, "traffic.log", "read " + connectionSocket.getRemoteSocketAddress());
        return input;

    }

    public String readCommand() throws IOException {
        String input = readLine();
        if (input != null) {

            if (input.contains("@")) {
                input = input.replace("@", "");
                return input;
            } else {
                throw new IOException("Not implemented"); // not really the way to do it bu anyway
            }
        }
        // TODO : To better implement
        throw new IOException("Not implemented");
    }

    public byte[] readDataWithLenght() throws Exception {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        byte[] bytesInt = null;

        int readBytes = inDataFromClient.read(bytesInt, 0, 4);

        if (readBytes != 4) {
            App.logProgram("Didn't read 4 bytes but " + readBytes);
        }
        bb.put(bytesInt);
        int totalBytes = bb.getInt(0);


        byte[] buffer = new byte[totalBytes];
        int bytesRead = 0;
        int chunk;
        while (bytesRead < totalBytes) {
            chunk = inDataFromClient.read(buffer, bytesRead, buffer.length - bytesRead);
            if (chunk == 0) {
                throw new Exception("Unespected disconnect");
            }
            bytesRead += chunk;
        }
        return buffer;


    }

    public void writeDataWithLenght(byte[] content) throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(4);
        // because Java store data as BigEndian where .NET store them in LittleEndian
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putInt(content.length);
        System.out.println("Data length is : " + content.length);

     //   outToClient.write(bb.array(), 0, 4);
    //    outToClient.flush();
       writeLine(String.valueOf( content.length));

        System.out.println("outToClient.size : " +  outToClient.size());
        outToClient.write(content, 0, content.length);

        System.out.println("outToClient.size : " +  outToClient.size());
   //     outToClient.flush();

        System.out.println("outToClient.size : " +  outToClient.size());

        String recieve = readCommand();
        if (recieve.equals("RECIVED")) {
            writeCommand("SENDED_OK");
            App.logProgram("Well used");
        } else {
            writeCommand("SENDED_KO");
            App.logProgram("Recieved bad elements");
        }

    }


    public void stop() throws IOException {
        connectionSocket.close();
    }
}
