package fr.henri2h.Server;

import java.util.List;
import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import fr.henri2h.Shared.App;

public class PushNotifactionHelper {

    public static void sendPushNotificationToDevices(List<String> DevicesID, String title, String content) {
        for (String DeviceID : DevicesID) {
            // System.out.print("Notification for : " + DeviceID);
            sendPushNotification(DeviceID, title, content);
        }
    }

    public static void sendPushNotification(String DeviceID, String title, String content) {
        String id = Server.getFirebaseID();
        if (id != null) {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
            post.setHeader("Content-type", "application/json");
            post.setHeader("Authorization", "key=" + id);
            JSONObject message = new JSONObject();
            message.put("to", DeviceID);
            message.put("priority", "high");

            JSONObject notification = new JSONObject();
            notification.put("title", title);
            notification.put("body", content);
            notification.put("sound", "defautl");

            message.put("data", notification); // according to https://github.com/phonegap/phonegap-plugin-push/blob/master/docs/PAYLOAD.md#notification-vs-data-payloads

            post.setEntity(new StringEntity(message.toString(), "UTF-8"));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            App.logError("Firebase key not set, can't send push notifications");
        }
    }
}