package fr.henri2h.Server;

import fr.henri2h.Server.ObjectsManagers.ConversationManager;
import fr.henri2h.Server.ObjectsManagers.UsersManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;

import fr.henri2h.Shared.Objects.*;
import fr.henri2h.Shared.FileManager;
import fr.henri2h.Shared.App;

public class Server {
    public Connection conn = null;

    public UsersManager usersManager;
    public ConversationManager conversationManager;

    public Server() {
        // first we will check if database exist
        Connect();
        CreateTables();

        usersManager = new UsersManager(conn);
        conversationManager = new ConversationManager(conn);
        List<String> devices_id = GetDeviceID("henri2h");
        PushNotifactionHelper.sendPushNotificationToDevices(devices_id, "CChat", "fr.henri2h.Server started");
        // At least, we can send push notification to indicate that the server has
        // started
        // PushNotifactionHelper.sendPushNotification("cDHGxDeY-LU:APA91bEAlqZyABrS-YK-MpsAAO2IKZctviTr5Dg-HxGSUMR0LTtJr5DuOntBY3Whm8vas5JITpdNss8PY0FJ28aOyh8LnR5vBlXs33RaFJ-5NKthyn4vIdgohZJq4Aal0fAoqePNW__6",
        // "Title", "Update");
    }

    public void CloseServer() {
        try {
            conn.close();
            System.exit(0);
        } catch (SQLException e) {
            App.logError("SqlException while closing the server",e.getMessage());
        }
    }

    public void Connect() {
        try {
            // db parameters
            String url = "jdbc:sqlite:server_database.db";

            // create a connection to the database
            conn = DriverManager.getConnection(url);
            App.log("Connection to SQLite has been established.");

        } catch (SQLException e) {
            App.logError("SqlException while connection to the sql database",e.getMessage());
        }
    }

    public void ExecuteSQL(String sql) {
        try {
            Statement stmt = conn.createStatement();

            // create a new table
            stmt.execute(sql);

        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public void CreateTables() {
        String sql = "CREATE TABLE IF NOT EXISTS users " + "(name TEXT NOT NULL, "
                + " username TEXT  UNIQUE   NOT NULL, " + " password TEXT, password_hash TEXT, " + " pictures_id TEXT, " + " token TEXT, "
                + " token_update_date TEXT)";
        ExecuteSQL(sql);

        // Conversation table
        sql = "CREATE TABLE IF NOT EXISTS conversations (" + "id TEXT NOT NULL UNIQUE, " + "name TEXT NOT NULL, "
                + "picture_id  TEXT, last_message_user TEXT, last_message_text TEXT, last_message_datatype TEXT, last_message_sent_date TEXT, last_message_id INTEGER" + ");";
        ExecuteSQL(sql);

        // table to say wich user is in the conversation
        sql = "CREATE TABLE IF NOT EXISTS conversations_list"
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL, username TEXT NOT NULL, "
                + " conv_id TEXT NOT NULL, " + " join_date TEXT)";
        ExecuteSQL(sql);

        sql = "CREATE TABLE IF NOT EXISTS pictures " + "(pictures_id TEXT UNIQUE NOT NULL, "
                + " pictures_data BLOB NOT NULL)";
        ExecuteSQL(sql);

        // in order to be able to send push notifications
        sql = "CREATE TABLE IF NOT EXISTS client_device_id "
                + "(username TEXT NOT NULL, device_id TEXT NOT NULL UNIQUE, date_added TEXT, date_last_connection TEXT NOT NULL)";
        ExecuteSQL(sql);
        // prevent the user to add the same device id multiple times but allow one
        // device to be configured for two usernames
        // may be not realy clever ...
        // sql = "alter table client_device_id add UNIQUE (username, device_id)";
        // ExecuteSQL(sql);

        sql = "CREATE TABLE IF NOT EXISTS setting " + "(Firebase_authorisation_key TEXT NOT NULL)";
        ExecuteSQL(sql);

    }

    public boolean AddUser(User user) {
        return usersManager.AddUser(user);
    }

    public List<ConversationListItem> ListConversations(String username) {
        return conversationManager.ListConversations(username);
    }
    
    public List<String> ListUsers() {
        return usersManager.ListUsers();
    }

    public List<String> SearchForUser(String username, String searchedUsername){
        return conversationManager.SearchForUsers(username, searchedUsername);
    }

    public void SetConversationName(String convID, String convName) {
        conversationManager.SetConversationName(convID, convName);
    }

    public boolean CreateConversation(String convName, String[] usernames) {
        return conversationManager.CreateConversation(convName, usernames);
    }

    public boolean AddConversationMessage(ConversationMessage message) {
        return conversationManager.AddConversationMessage(message);
    }

    public boolean CanUserAccessConversation(String username, String convID) {
        return true;
    }

    public boolean CanUserAccessConversation(String username, String token, String convID) {
        return true;
    }

    public Conversation GetConversation(String conversationID) {
        return conversationManager.GetConversation(conversationID);
    }

    public String GetConversationName(String convID){
        return conversationManager.GetConversationName(convID);
    }

    public List<String> GetUsersInConversation(String convID) {
        return conversationManager.GetUsersInConversation(convID);
    }

    public List<String> GetDeviceID(String username) {
        List<String> DevicesID = new ArrayList<>();
        // sql query
        String sql = "SELECT device_id FROM client_device_id WHERE username = ?";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, username);
            ResultSet rs = pstmt.executeQuery();

            // loop through the result set
            while (rs.next()) {
                DevicesID.add(rs.getString("device_id"));
            }
            return DevicesID;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        // error
        return new ArrayList<>();
    }

    public boolean AddDeviceID(String username, String clientID) {
        System.out.println(username + " is going to add the device id : " + clientID);

        if (username.equals(null) || username.equals("") || clientID.equals(null) || clientID.equals("")) {
            return false;
        }
        try {
            String sql = "INSERT INTO client_device_id (username, device_id, date_added, date_last_connection) VALUES (?,?,?,?)";

            Date date = Calendar.getInstance().getTime();
            final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, clientID);
            stmt.setString(3, dateFormat.format(date));
            stmt.setString(4, dateFormat.format(date));
            stmt.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }

    }

    public boolean setFirebaseID(String id) {
        try {
            String[] array_config = { id };
            FileManager.setFileContent("firebase_key", id);
            return true;
        } catch (Exception e) {
            App.saveError("Could not set Firebase ID", e);
            return false;
        }
    }

    public static String getFirebaseID() {
        try {
            List<String> content = FileManager.loadFile(null, "firebase_key");
            App.log("Firebase key : |" + content.get(0) + "|");
            return content.get(0);
        } catch (Exception e) {
            App.saveError("Could not get Firebase ID", e);
            return null;
        }
    }

	public boolean DeleteConversation(String convID) {
		return conversationManager.DeleteConversation(convID);
	}
}
