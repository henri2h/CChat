package fr.henri2h.Server.ObjectsManagers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.henri2h.Shared.App;

import fr.henri2h.Shared.Objects.User;

public class UsersManager {
    Connection conn;

    public UsersManager(Connection conn_in) {
        conn = conn_in;
    }

    public boolean AddUser(User user) {
        String sql = "INSERT INTO users (username, name, password) VALUES(?,?,?)";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, user.Username.toLowerCase());
            pstmt.setString(2, user.Name);
            pstmt.setString(3, user.Password);
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public List<String> ListUsers() {
        List<String> usernames = new ArrayList<>();
        String sql = "SELECT username FROM users";
        try (Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            // loop through the result set
            while (rs.next()) {
                usernames.add(rs.getString("username"));
            }
        } catch (SQLException e) {
            App.logError("[ Users Manager ] : could not get users", e);
            return null;
        }
        return usernames;
    }
}
