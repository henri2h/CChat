package fr.henri2h.Server.ObjectsManagers;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import fr.henri2h.Shared.App;
import fr.henri2h.Shared.FileManager;
import fr.henri2h.Shared.Objects.Conversation;
import fr.henri2h.Shared.Objects.ConversationListItem;
import fr.henri2h.Shared.Objects.ConversationMessage;
import fr.henri2h.Shared.Objects.DataType;

public class ConversationManager {
    Connection conn;

    public ConversationManager(Connection conn_in) {
        conn = conn_in;
    }

    public List<ConversationListItem> ListConversations(String username) {
        List<ConversationListItem> convList = new ArrayList<>();

        // sql query
        String sql = "SELECT name, conversations.id as cid, last_message_user, last_message_text, last_message_datatype, last_message_sent_date, last_message_id, picture_id"
                + " FROM conversations JOIN conversations_list ON conversations_list.conv_id = cid WHERE username = ?";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, username);
            //
            ResultSet rs = pstmt.executeQuery();

            // loop through the result set
            while (rs.next()) {
                // create item
                ConversationListItem cl = new ConversationListItem(rs.getString("cid"), rs.getString("name"));
                cl.last_message_sent_date = rs.getString("last_message_sent_date");
                cl.last_message_datatype = rs.getString("last_message_datatype");
                cl.last_message_text = rs.getString("last_message_text");
                cl.last_message_user = rs.getString("last_message_user");
                cl.last_message_id = rs.getString("last_message_id");
                cl.picture_id = rs.getString("picture_id");
                
                convList.add(cl);
            }

            return convList;
        } catch (SQLException e) {
            App.logError("[ ConversationManager ] : could not list conversation for user : " + username, e);
        }
        return new ArrayList<>();
    }

    public void SetConversationName(String convID, String convName) {
        String sql = "UPDATE conversations SET name = ? " + "WHERE id = ?";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, convName);
            pstmt.setString(2, convID);
            // update
            pstmt.executeUpdate();
        } catch (SQLException e) {
            App.logError("[ ConversationManager ] :  could not set conversation " + convID + " to : " + convName, e);
        }
    }

    public String GetConversationName(String convID) {
        String sql = "SELECT name from conversations WHERE id = ?";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, convID);
            //
            ResultSet rs = pstmt.executeQuery();

            // loop through the result set
            while (rs.next()) {
                return rs.getString("name");
            }
        } catch (SQLException e) {
            App.logError("[ ConversationManager ] : could not get conversation name " + convID, e);
        }
        return "";
    }

    public List<String> GetUsersInConversation(String convID) {
        List<String> usernames = new ArrayList<>();

        if (convID.equals("") || convID.equals(null)) {
            return null;
        }

        String sql = "SELECT username, conv_id FROM conversations_list WHERE conv_id = ?";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, convID);

            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                usernames.add(rs.getString("username"));
            }
        } catch (SQLException e) {
            App.logError("[ ConversationManager ] : could not get users in conversation " + convID, e);
            return null;
        }
        return usernames;
    }

    public List<String> SearchForUsers(String username, String searchedUsername) {
        List<String> usernames = new ArrayList<>();

        if (searchedUsername.equals("") || searchedUsername.equals(null)) {
            return null;
        }

        String sql = "SELECT username FROM users WHERE username LIKE ?";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, '%' + searchedUsername.toLowerCase() + '%');

            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                usernames.add(rs.getString("username"));
            }
        } catch (SQLException e) {
            App.logError("[ ConversationManager ] : could not search for user " + searchedUsername + " ordored by "
                    + username, e);
            return null;
        }
        return usernames;
    }

    public Conversation GetConversation(String conversationID) {
        if (conversationID.equals("") || conversationID.equals(null)) {
            return null;
        }
        String ConvName = GetConversationName(conversationID);
        List<ConversationMessage> Messages = new ArrayList<>();

        String sql = "SELECT * FROM conversation_" + conversationID;

        try (Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                int ID = rs.getInt("id");
                String username = rs.getString("username");
                String content = rs.getString("content");
                String date = rs.getString("send_date");
                DataType dt = DataType.parse(rs.getString("datatype"));

                ConversationMessage cm = new ConversationMessage(ID, conversationID, username, content);
                cm.SendDate = date;
                cm.DataType = dt;
                Messages.add(cm);
            }
        } catch (SQLException e) {
            App.logError("[ ConversationManager ] : could not get conversation " + conversationID, e);
        }

        Conversation conv = new Conversation(conversationID, conversationID, Messages);
        conv.Usernames = GetUsersInConversation(conversationID).toArray(new String[0]);
        return conv;
    }

    public boolean CreateConversation(String convName, String[] users) {
        // add conversation
        App.log("Going to create a conversation");
        try {
            // generate convID

            final String convID = UUID.randomUUID().toString().replace("-", "_");

            // create table
            String sql = "CREATE TABLE IF NOT EXISTS conversation_" + convID
                    + "(id       INTEGER  PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, " + "username  TEXT     NOT NULL,"
                    + "content   TEXT     NOT NULL," + "datatype   TEXT     NOT NULL," + "send_date TEXT     NOT NULL)";

            Statement statement = conn.createStatement();
            boolean result = statement.execute(sql);

            // add to conversations table
            sql = "INSERT INTO conversations (id, name) VALUES(?,?)";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, convID);
            stmt.setString(2, convName);
            stmt.executeUpdate();

            // add conversation list
            sql = "INSERT INTO conversations_list (conv_id, username) VALUES(?,?)";

            for (String user : users) {
                PreparedStatement pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, convID);
                pstmt.setString(2, user);

                pstmt.executeUpdate();
            }

            App.log("Conversation " + convID + " created");
            return true;

        } catch (SQLException e) {
            App.logError("[ ConversationManager ] :  could not create a conversation", e);
            return false;
        }
    }

    public boolean AddConversationMessage(ConversationMessage message) {
        if (message.equals(null) || message.ConvID.equals("") || message.ConvID.equals(null)) {
            return false;
        }
        try {
            String table_name = "conversation_" + message.ConvID.replace("/", ""); // /!\ SQL injection vulnerability
                                                                                   // !!!!!

            String sql = "INSERT INTO " + table_name + " (username, content, datatype, send_date) VALUES (?,?,?,?)";

            Date date = Calendar.getInstance().getTime();
            final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, message.Username);
            stmt.setString(2, message.Content);
            stmt.setString(3, message.DataType.toString());
            stmt.setString(4, dateFormat.format(date));
            int a = stmt.executeUpdate();

            // DEBUG : to understand what doest stmt.executeUpdate return and if it is
            // different from ID

            // update conversations_list table :
            sql = "UPDATE conversations set last_message_user=?, last_message_text=?, last_message_sent_date=?, last_message_id=?, last_message_datatype=? where id=?";

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, message.Username);
            if (message.DataType == DataType.Text)
                stmt.setString(2, message.Content);
            else if (message.DataType == DataType.Image)
                stmt.setString(2, "Image sent");
            else if (message.DataType == DataType.File)
                stmt.setString(2, "File sent");
            else
                stmt.setString(2, "Error : unknown data type");

            stmt.setString(3, dateFormat.format(date));
            stmt.setInt(4, a);
            stmt.setString(5, message.DataType.toString());
            stmt.setString(6, message.ConvID);
            stmt.executeUpdate();

            return true;
        } catch (SQLException e) {
            App.logError("[ ConversationManager ] :  could not add a conversation message", e);
            return false;
        }
    }

    public boolean DeleteConversation(String convID) {
        // add to conversations table

        try {
            App.log("Deleting : " + convID);

            // create table
            String table_name = "conversation_" + convID.replace("/", ""); // /!\ SQL injection vulnerability
            // !!!!!

            String sql = "DROP TABLE " + table_name + ";";

            Statement statement = conn.createStatement();
            boolean result = statement.execute(sql);

            sql = "DELETE FROM conversations where id=?";

            PreparedStatement stmt;
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, convID);
            stmt.executeUpdate();

            // delete conversation list
            sql = "DELETE FROM conversations_list where conv_id=?";

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, convID);
            stmt.executeUpdate();

            // TODO : delete all files

            return true;

        } catch (SQLException e) {
            App.logError("Could not delete conversation : " + convID, e);
            return false;
        }
    }

    public boolean CleanConversationDatabase() {
        // delete reference to emty databases
        String sql = "DELETE FROM conversations_list WHERE conv_id IN (SELECT DISTINCT conv_id FROM conversations_list WHERE ('conversation_' || conv_id) NOT IN (SELECT name FROM sqlite_master WHERE type='table'))";
        try {
            Statement statement = conn.createStatement();
            boolean result = statement.execute(sql);
            return true;
        } catch (SQLException e) {
            App.logError("Could not clean the conversations database", e);
            return false;
        }
    }
}
