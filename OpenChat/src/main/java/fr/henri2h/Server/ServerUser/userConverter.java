package fr.henri2h.Server.ServerUser;

import fr.henri2h.Shared.User;

import java.util.ArrayList;

/**
 * Created by Henri on 24-Jul-16.
 *
 * @author Gabe
 * @author Henri2h
 */
public class userConverter {
    public static User serverUserToUser(ServerUser serverUser) {
            User user = new User();
            user.username = serverUser.username;
            user.FullName = serverUser.getName();
            user.imagePath = serverUser.getImagePath();
            user.version = serverUser.getVersion();
        return user;
    }
    public static ArrayList<User> serverUsersToUsers(ArrayList<ServerUser> serverUsers) {
        ArrayList<User> users = new ArrayList<>();
        System.out.println("Size of the users :" + users.size());
        for (ServerUser serverUser : serverUsers) {
            User user = new User();
            user.username = serverUser.username;
            user.FullName = serverUser.getName();
            user.imagePath = serverUser.getImagePath();
            user.version = serverUser.getVersion();

            users.add(user);
        }
        return users;
    }

}
