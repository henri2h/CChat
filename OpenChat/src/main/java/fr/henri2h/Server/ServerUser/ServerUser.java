package fr.henri2h.Server.ServerUser;

import fr.henri2h.Shared.App;
import fr.henri2h.Shared.FileManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henri on 22-Jul-16.
 *
 * @author Gabe
 * @author Henri2h
 */
public class ServerUser {
    public ServerUser(String Username) {
        username = Username;
    }

    // this is static
    public final String username;

    private String[] userInfo() {
        if (userInfos == null) {

            try {
                List<String> userInfo = FileManager.loadFile(new String[]{"users", username}, "information");
                if (userInfo != null) {
                    return userInfo.toArray(new String[0]);
                } else {
                    return new String[4];
                }
            } catch (IOException e) {
                App.logError("Error in loading the user : " + username,e);
            }

            return new String[4];
        } else {
            return userInfos;
        }
    }


    private String[] userInfos;


    public String getName() {
        return userInfo()[0];
    }
    public String getPassword() {
        return userInfo()[1];
    }
    public String getImagePath() {
        return userInfo()[2];
    }
    public String getVersion() {
        return userInfo()[3];
    }


    public List<String> getConversationID() {
        return conversationID;
    }

    private final List<String> conversationID = new ArrayList<>();

    public String[] getConversation() {
        try {
            if (FileManager.exist(new String[]{"users", username}, "conversations")) {

                List<String> convList = FileManager.loadFile(new String[]{"users", username}, "conversations");
                if (convList != null) {
                    String[] elements = new String[convList.size()];
                    for (int i = 0; i < convList.size(); i++) {
                        elements[i] = convList.get(i);
                    }
                    return elements;
                }
            }
            // not enough items
            return new String[]{"empty"};

        } catch (IOException e)
        {
            App.logError("Error in reading the conversation for the user : " + username, e);
        }
        App.logProgram("Going to be null...");
        return null;
    }


    public void addConversationID(String convID) {

        conversationID.add(convID);
        try {
            FileManager.appendTextWithNewLine(new String[]{"users", username}, "conversations", convID);
        } catch (IOException e) {
            App.logError("Error in opening the file", e);
        }
    }

}
