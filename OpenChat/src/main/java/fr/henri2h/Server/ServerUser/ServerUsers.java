
package fr.henri2h.Server.ServerUser;

import fr.henri2h.Shared.App;
import fr.henri2h.Shared.FileManager;
import fr.henri2h.Shared.User;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ServerUsers {
    public static ArrayList<ServerUser> serverUsers;
    private static final String main = Paths.get(".").toAbsolutePath().normalize().toString() + "/users/";
    private static boolean isLoaded = false;

    public ServerUsers() {
        loadUsers();
    }

    public static void loadUsers() {
        try {
            if (!FileManager.exist(new String[]{"users"}, "users.info")) {
                serverUsers = new ArrayList<>();
                App.logProgram("There is no users");
            } else {
                ArrayList<ServerUser> loadedUsers = new ArrayList<>();
                Path myFile = Paths.get(main + "users.info");
                List<String> content = Files.readAllLines(myFile, Charset.defaultCharset());

                for (String username : content) {

                    ServerUser user = new ServerUser(username);

                    App.logProgram("Loaded user " + user.username + " with name : " + user.getName() + " password : "
                            + user.getPassword() + " " + user.getImagePath());

                    loadedUsers.add(user);
                }
                serverUsers = loadedUsers;
                isLoaded = true;
                App.logProgram("Loaded users : " + serverUsers.size());
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }

    }

    public static boolean addUser(String username) {
        try {
            if (username.matches("(\\w+)")) {
                FileManager.appendTextWithNewLine(new String[]{"users"}, "users.info", username);
                String[] content = new String[]{username, "", "", "1"};
                FileManager.writeText(new String[]{"users", username}, "information", content);
                return true;
            } else {
                App.logError("Illegal character in the username : " + username);
            }
        } catch (IOException e) {
App.logError("Error in adding the user : " + username,e);
        }
        return false;
    }

    public static ArrayList<ServerUser> getUsers() {
        if (!isLoaded) {
            loadUsers();
        }
        return serverUsers;
    }

    public static User getUser(String usernameRequested) {
        int userPos = findUser(usernameRequested);

        //userPos = -1 if the requested user is not valid (didn't exist)
        if (userPos != -1) {
            return userConverter.serverUserToUser(serverUsers.get(userPos));
        } else {
            return null;
        }
    }

    public static ServerUser getServerUser(String usernameRequested) {
        int userPos = findUser(usernameRequested);

        //userPos = -1 if the requested user is not valid (didn't exist)
        if (userPos != -1) {
            return serverUsers.get(userPos);
        } else {
            return null;
        }
    }


    public static byte[] getImageUser(String usernameRequested) {
        try {
            Path userPath = FileManager.getPath(new String[]{"users", usernameRequested}, getServerUser(usernameRequested).getImagePath());
            return Files.readAllBytes(userPath);
        } catch (IOException e) {
            return null;
        }
    }

    private static int findUser(String userRequested) {
        for (ServerUser user : serverUsers) {
            if (user.username.equals(userRequested)) {
                return serverUsers.indexOf(user);
            }
        }
        return -1;
    }

    public static boolean userExist(String username) {
        for (ServerUser user : serverUsers) {
            if (user.username.equals(username)) {
                return true;
            }

        }
        return false;
    }


}
