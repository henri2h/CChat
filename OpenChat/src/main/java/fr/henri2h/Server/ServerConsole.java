package fr.henri2h.Server;

import fr.henri2h.Server.ServerUser.ServerUsers;
import fr.henri2h.Server.Servers.TCPServer.TCPServer;
import fr.henri2h.Server.conversationsTools.conversationManager;
import fr.henri2h.Shared.App;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by henri on 02/02/17.
 */
public class ServerConsole {
    public static void printHelp() {
        String[] linesToPrint = new String[]{
                "Help :",
                "In order to add a user : ",
                "   - add user <username>",
                "",
                "In order to add a conversation : ",
                "   - add conversation <userA userB ... userEnd>",
                "",
                "Stop the server :",
                "   - stop"

        };

        for (String sIn : linesToPrint) {
            System.out.println(sIn);
        }
    }

    public static void interprete() {

        Scanner scan = new Scanner(System.in);
        String input = "";
        while (!input.equals("stop")) {
            System.out.print("Command > ");
            input = scan.nextLine();
            String[] commandParts = input.split(" ");

            if (input.equals("help")) {
                ServerConsole.printHelp();

            } else if (input.equals("stop")) {
                App.logProgram("Stopping the server");
              TCPServer.stopServer();
                break;
            } else if (commandParts.length >= 3) {
                if (commandParts[0].equals("add") && commandParts[1].equals("user")) {
                    String username = commandParts[2];
                    boolean result = ServerUsers.addUser(username);
                    if (result) System.out.println("Added " + username);
                } else if (commandParts[0].equals("add") && commandParts[1].equals("conversation")) {

                    List<String> users = new ArrayList<>();

                    for (int i = 2; i < commandParts.length; i++) {
                        users.add(commandParts[i]);
                    }

                    conversationManager.createConversation(users, "conversation");
                } else {
                    System.out.println("Not a good arguments");
                }

            } else {
                System.out.println("Not a command");
            }
        }

    }
}
