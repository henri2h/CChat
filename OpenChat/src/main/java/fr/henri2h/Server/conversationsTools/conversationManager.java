package fr.henri2h.Server.conversationsTools;

import fr.henri2h.Server.ServerUser.ServerUser;
import fr.henri2h.Server.ServerUser.ServerUsers;
import fr.henri2h.Server.conversationsTools.conversationElements.conversationElement;
import fr.henri2h.Server.conversationsTools.conversationElements.conversationElementInformation;
import fr.henri2h.Shared.App;
import fr.henri2h.Shared.FileManager;
import fr.henri2h.Shared.TCPConversation.TCPConversationElement;
import fr.henri2h.Shared.TCPConversation.TCPConversationMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Created by henri on 27/11/16.
 */
public class conversationManager {

    public static conversationElementManager convElementManager = new conversationElementManager();

    public static String[] listConversations() {
        return FileManager.listFiles(new String[]{"conversations"});
    }

    public static boolean createConversation(List<String> users, String name) {
        boolean userExist = true;
        for (String s : users) {
            if (!ServerUsers.userExist(s)) {
                userExist = false;
            }

        }

        if (userExist) {

            conversationElement conv = new conversationElement();
            conv.conversationInformation = new conversationElementInformation();
            conv.conversationMessages = new ArrayList<>();

            //todo : change this loop to a do while
            String convID = UUID.randomUUID().toString();
            while (conversationElementManager.conversationExist(convID)) {
                convID = UUID.randomUUID().toString();
            }
            // generate the conversation name
            conv.setConvID(convID);
            conv.conversationInformation.convID = convID;
            conv.conversationInformation.name = name;
            // add the users
            conv.conversationInformation.users = new ArrayList<>();

            conv.conversationInformation.users.addAll(users);
            try {
                conv.createMessageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (String s : users) {
                if (ServerUsers.userExist(s)) {
                    ServerUser sUser = ServerUsers.getServerUser(s);

                    if (sUser != null) {
                        sUser.addConversationID(conv.conversationInformation.convID);
                    }
                }

            }
            App.log("conversation " + convID + " was created ");
            return conv.save();
        }
        return false;
    }

    public static boolean canAccess(String username, String convID) {
        if (ServerUsers.userExist(username)) {
            conversationElement cElem = getConversation(convID, false);
            for (String user : cElem.conversationInformation.users) {
                if (user.equals(username)) {
                    return true;
                }
            }
        }
        return false;
    }


    static conversationElement getConversation(String convID, boolean loadMessages) {
        try {
            return conversationElementManager.getConversation(convID, loadMessages);
        } catch (Exception e) {
            String error = "Conversation " + convID + "not found at getConversation";
            App.logError(error, e);
            return null;
        }
    }




    public static String getConversationName(String convID) {
        if (conversationElementManager.conversationExist(convID)) {
            conversationElement conv;
            try {
                conv = conversationElementManager.getConversationInformations(convID);
            } catch (Exception e) {
                String error = "cannot get the name of the conversation " + convID;
                App.logError(error, e);
                return error;
            }

            return conv.getConvName();

        } else {
            return "conversation didn't exist";
        }
    }

    public static void setConversationName(String convName, String convID) {
        try {
            conversationElement convElem    = conversationElementManager.getConversationInformations(convID);
            convElem.setConvName(convName);

        } catch (Exception e) {
            String error = "Conversation " + convID + "not found at getConversation";
            App.logError(error, e);
        }
    }
    // TODO : remake this function to add the message directly in the file and to add the number
    public static void appendTCPMessage(TCPConversationMessage message) {
        try {
            conversationElementManager.addMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // for the client : cast conversationElement to TCPConversation
    public static TCPConversationElement getTCPConversation(String convID) {
        conversationElement conv;
        try {
            conv = conversationElementManager.getConversationWithMessages(convID);


            TCPConversationElement tcpConv = new TCPConversationElement();
            tcpConv.convID = convID;
            tcpConv.convName = conv.conversationInformation.name;
            tcpConv.numberOfMessage = conv.conversationInformation.numberOfMessages;

            // users
            String[] convUsers = new String[conv.conversationInformation.users.size()];
            for (int i = 0; i < conv.conversationInformation.users.size(); i++) {
                convUsers[i] = conv.conversationInformation.users.get(i);
            }

            tcpConv.users = convUsers;

            tcpConv.messages = new ArrayList<>();
            if (conv.conversationMessages != null) {
                for (TCPConversationMessage mess : conv.conversationMessages) {
                    tcpConv.messages.add(mess);
                }
            }

            // // TODO: 28/11/16 implement the function which will search for all the conversation and return an array with all the TCPConversation with the name, the participent, and the 20 latest messages
            return tcpConv;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
