package fr.henri2h.Server.conversationsTools;

import fr.henri2h.Server.conversationsTools.conversationElements.conversationElement;
import fr.henri2h.Shared.App;
import fr.henri2h.Shared.FileManager;
import fr.henri2h.Shared.TCPConversation.TCPConversationMessage;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Henri on 30-Jan-17.
 */
public class conversationElementManager {
    static Map<String, conversationElement> convElement = new HashMap<String, conversationElement>();

    public conversationElementManager() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    cleanList();
                } catch (Exception e) {
                    App.logError("Error in timer : cleaning the conversation", e);
                }
            }
        }, 60 * 1000, 10 * 60 * 1000);

    }

    // TODO: load only the last message because the big conversation will be too heavy to load and will consume all the memory
    public static conversationElement loadElement(String convID) throws Exception {
        conversationElement cElem = new conversationElement();
        cElem.setConvID(convID);
        cElem.loadConversation();
        convElement.put(convID, cElem);
        return cElem;
    }

    // TODO: enhance this method to call it when the program consume to much memory
    public static void cleanList() {
        LocalDateTime tenFormNow = LocalDateTime.now();
        tenFormNow.plusMinutes(-10);

        for (conversationElement c : convElement.values()) {
            // save all the conversation
            c.save();

            // remove all conversation the old conversation
            if (c.lastAccessed.isBefore(tenFormNow)) {
                App.logProgram("Removing old data : " + c.getConvID());
                convElement.remove(c.getConvID());
            }

        }
    }

    public static boolean isConvLoaded(String convID) {
        return convElement.containsKey(convID);
    }

    public static void addMessage(TCPConversationMessage message) throws Exception {
        conversationElement convUser = getConversation(message.convID, false);
        convUser.addConversationMessage(message);
    }

    public static conversationElement getConversation(String convID, boolean loadMessages) throws Exception {
        conversationElement elem;

        if (isConvLoaded(convID)) {
            elem = convElement.get(convID);
        } else {
            elem = loadElement(convID);
        }

        if (loadMessages) {
            elem.loadConversationMessages(20);
        }

        elem.lastAccessed = LocalDateTime.now();
        convElement.replace(convID, elem);
        return elem;

    }

    public static conversationElement getConversationInformations(String convID) throws Exception {
        return getConversation(convID, false);
    }

    public static conversationElement getConversationWithMessages(String convID) throws Exception {
        return getConversation(convID, true);
    }

    public static boolean conversationExist(String convID) {
        return FileManager.exist(new String[]{"conversations", convID});
    }

}

