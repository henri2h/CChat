package fr.henri2h.Server.conversationsTools.conversationElements;

import fr.henri2h.Shared.App;
import fr.henri2h.Shared.FileManager;
import fr.henri2h.Shared.TCPConversation.TCPConversationMessage;
import fr.henri2h.Shared.TCPConversation.messageContent;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henri on 30-Jan-17.
 */
public class conversationElement {

    //conversation data
    public conversationElementInformation conversationInformation;
    public List<TCPConversationMessage> conversationMessages;

    // local class
    public LocalDateTime lastAccessed;
    private String convID;
    private boolean loaded = false;

    private String conversationInformationFileName = "conversationInformations";
    private String conversationMessagesFileName = "conversationMessages";
    private String convName;

    public String getConvID() {
        if (conversationInformation != null) return conversationInformation.convID;
        return null;
    }

    public void setConvID(String convID) {
        this.convID = convID;
    }

    public void loadConversation() {
        try {
            loadConversationInformation();
            //loadConversationMessages(20);
        } catch (Exception e) {
            App.logError("Conversation " + convID + " not loaded", e);
        }

        lastAccessed = LocalDateTime.now();

    }


    private void loadConversationInformation() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File file = FileManager.getFile(new String[]{"conversations", convID}, conversationInformationFileName);
        conversationInformation = mapper.readValue(file, conversationElementInformation.class);
    }

    public void loadConversationMessages(int number) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        File file = FileManager.getFile(new String[]{"conversations", convID}, conversationMessagesFileName);
        conversationMessages = new ArrayList<>();

        if (!file.exists()) {
            //  throw new Exception("File " + file.getPath() + " didn't exist in conversationElement.loadConversationMessages for conv " + convID);

        } else {
        /*    ReversedLinesFileReader reversedLinesFileReader = new ReversedLinesFileReader(file);
            List<String> messagesS = new ArrayList<>();
            try {
                for (int i = 0; i < number; i++) {
                    messagesS.add(reversedLinesFileReader.readLine());
                }

            } catch (Exception e) {
            }
            reversedLinesFileReader.close();

            for (int i = (messagesS.size() - 1); i >= 0; i--) {
                String messString = messagesS.get(i);
                if (messString != null) {
                    try {
                        TCPConversationMessage convMess = mapper.readValue(messString, TCPConversationMessage.class);
                        conversationMessages.add(convMess);
                    } catch (Exception e) {
                        App.logError("Handeled error in loading a message : [" + messString + "] at conversationElement",e);
                    }
                }
            }*/

        }


    }

    public void addConversationMessage(TCPConversationMessage message) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        message.messageID = conversationInformation.numberOfMessages;
        conversationInformation.numberOfMessages++;

        List<messageContent> mc = new ArrayList<>();
        for (int i = 0; i < message.MessageContent.size(); i++) {
            messageContent mess = message.MessageContent.get(i);

            if (mess.DataType == fr.henri2h.Shared.TCPConversation.dataType.image|| mess.DataType == fr.henri2h.Shared.TCPConversation.dataType.file) {
                if (mess.file != null) {
                    String fileName = "file" + message.messageID + "." + i + "." + mess.content;
                    Path path = FileManager.getPath(new String[]{"conversations", convID}, fileName, true, true);
                    Files.write(path, mess.file);

                    mess.content = fileName;
                    mess.file = null;
                    mc.add(mess);

                    continue;
                }
            }
            mc.add(mess);
        }

        String convJSON = mapper.writeValueAsString(message);
        FileManager.appendTextWithNewLine(new String[]{"conversations", convID}, conversationMessagesFileName, convJSON);

        save();
    }

    // save informations
    public boolean save() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String convJSON = mapper.writeValueAsString(conversationInformation);
            FileManager.writeText(new String[]{"conversations", convID}, conversationInformationFileName, new String[]{convJSON});
            return true;
        } catch (IOException e) {
            App.logError("error in saving the conversation", e);
        }
        return false;

    }

    public void createMessageFile() throws IOException {
        File file = FileManager.getFile(new String[]{"conversations", convID}, conversationInformationFileName,true);
        file.createNewFile();
    }
    public String getConvName() {
        return conversationInformation.name;
    }
    public void setConvName(String convName) {
        conversationInformation.name = convName;
        this.save();
    }
}
