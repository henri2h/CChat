package fr.henri2h.Shared.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class ConversationMessage {
    public String ConvID;
    public int MessageID;

    public String SendDate;
    public String Username; // username of the user which send the message

    public String Content; // text or file id
    public DataType DataType;


    //todo : create a method to format a TCPMessage element to json and make the opposite conversion
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ConversationMessage(int MessageID, String ConvID, String Username, String content) {
        this.MessageID = MessageID;
        this.ConvID = ConvID;
        this.Username = Username;
        this.Content = content;
    }

   public ConversationMessage(){
        
   }
}
