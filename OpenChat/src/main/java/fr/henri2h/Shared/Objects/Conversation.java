package fr.henri2h.Shared.Objects;

import java.util.List;

public class Conversation {
    public List<ConversationMessage> Messages;

    public String[] Usernames; // usernames of the user in the conversation
    public String ConvName;
    public String ConvID;

    public int NumberOfMessage; // really usefull ?

    public Conversation(String ConvID, String ConvName, List<ConversationMessage> Messages) {
        this.ConvID = ConvID;
        this.ConvName = ConvName;
        this.Messages = Messages;

        this.NumberOfMessage = 0; // for the moment
    }
}
