package fr.henri2h.Shared.Objects;

public enum DataType {
    Text,
    File,
    Image;

    public static DataType parse(String string) {
        if(string.equals("Text")){
            return Text;
        }
        else if(string.equals("File")){
            return File;
        }
        else if (string.equals("Image")){
            return Image;
        }
        return Text;
    }
}
