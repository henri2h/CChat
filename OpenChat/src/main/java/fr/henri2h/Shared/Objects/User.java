package fr.henri2h.Shared.Objects;

public class User {
    public String Username;
    public String Name;
    public String ImageID;
    public String Password;

    public User(String Username, String Name, String Password){
        this.Username = Username;
        this.Name = Name;
        this.Password = Password;
        this.ImageID = "";
    }
}
