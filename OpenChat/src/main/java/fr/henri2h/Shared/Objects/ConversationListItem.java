package fr.henri2h.Shared.Objects;

public class ConversationListItem {
    public String ConvName;
    public String ConvID;
	public String last_message_text;
	public String last_message_sent_date;
	public String last_message_id;
	public String picture_id;
	public String last_message_user;
	public String last_message_datatype;

    public ConversationListItem(String ConvID, String ConvName)
    {
        this.ConvID = ConvID;
        this.ConvName = ConvName;
    }
}
