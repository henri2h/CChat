package fr.henri2h.Shared;

import java.io.*;
import java.util.ArrayList;

/**
 * This class is used to encode and decode the user database that we will send to the users
 *
 * @author Henri
 * @author Gabe
 */

public class Conversion {
    public static byte[] toByte(ArrayList<User> obj) {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            ObjectOutputStream o = new ObjectOutputStream(b);
            o.writeObject(obj);

            return b.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static ArrayList<User> toUser(byte[] bytes) {
        try {
            ByteArrayInputStream b = new ByteArrayInputStream(bytes);
            ObjectInputStream o = new ObjectInputStream(b);
            return (ArrayList<User>) o.readObject();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
