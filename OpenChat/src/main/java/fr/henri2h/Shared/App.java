package fr.henri2h.Shared;


import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by henri on 01/02/17.
 */
public class App {
    public static void createNewLog() {

        File file = FileManager.getFile(new String[]{"log"}, "programLog");
        if (file.exists()) {
            file.delete();
        }

        logProgram("Creating the server");
    }

    public static void log(String text) {
        logData(text, "generalLog");
    }

    public static void logData(String text, String fileName) {
        logData(text, fileName, fileName);
    }

    public static void logData(String text, String fileName, String appName) {
        logData(text, fileName, appName, true);
    }

    public static void logMessages(String s) {
        logData(s, "messages");
    }

    public static void logError(String error, Exception e) {
        e.printStackTrace();

        String savePath = saveError(error, e);
        error = error + " | saved in : " + savePath;
        logError(error);
    }

    public static void logProgram(String text) {
        text = "[fr.henri2h.Server] : " + text;
        logData(text, "programLog");
        System.out.println(text);
    }

    public static void logError(String error) {
        logData(error, "error");
    }

    public static void logError(String error, String severity) {
        error = "[Error, severity : " + severity + "] : " + error;
        //System.out.println(error);
        logData(error, "error");
    }

    public static String saveError(String description, Exception e) {
        String errorName = "error";
        File file = FileManager.getFile(new String[]{"log", "errorSaves"}, errorName + ".err", true);

        int pos = 1;
        while (file.exists()) {
            String newErrorName = errorName + pos;
            file = FileManager.getFile(new String[]{"log", "errorSaves"}, newErrorName + ".err", true);
            pos++;
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            FileManager.writeText(file, new String[]{"New error : ", description, mapper.writeValueAsString(e)});
        } catch (IOException e1) {
            App.logError("could not save an error", "high");
            e1.printStackTrace();
        }
        return file.toString();
    }

    public static void logData(String text, String fileName, String appName, boolean createNewLine) {
        try {
            if (appName.equals(null) == false && appName.equals("") == false) {
                text = "[" + LocalDateTime.now().toString() + " : " + appName + "] : " + text;
            } else {
                text = "[" + LocalDateTime.now().toString() + "] : " + text;
            }

            System.out.println(text);

            if (createNewLine) {
                FileManager.appendTextWithNewLine(new String[]{"log"}, fileName, text);
            } else {
                FileManager.appendText(new String[]{"log"}, fileName, text);
            }

        } catch (IOException e) {
            System.out.println("Could not save in logfile :  " + text);
            e.printStackTrace();
        }
    }
}
