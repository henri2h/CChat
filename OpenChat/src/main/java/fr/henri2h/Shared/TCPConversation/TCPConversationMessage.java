package fr.henri2h.Shared.TCPConversation;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

/**
 * Created by henri on 28/11/16.
 */
public class TCPConversationMessage {

    public String convID;
    public int messageID;

    public String sendDate;
    public String username;

    public List<messageContent> MessageContent;

    //todo : create a method to format a TCPMessage element to json and make the opposite conversion
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
