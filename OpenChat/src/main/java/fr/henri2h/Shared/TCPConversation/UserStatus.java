package fr.henri2h.Shared.TCPConversation;

import java.util.Date;

/**
 * Created by Henri on 30-Jan-17.
 */
public class UserStatus {
    public String username;
    public int readMessageID; // get the reading position of a user
    public Date readDate;
}
