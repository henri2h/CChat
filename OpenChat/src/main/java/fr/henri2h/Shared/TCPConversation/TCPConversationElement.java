package fr.henri2h.Shared.TCPConversation;

import java.util.List;

/**
 * Created by Henri on 03-Feb-17.
 */
public class TCPConversationElement {
    public List<TCPConversationMessage> messages;

    public String[] users;
    public String convName;
    public String convID;

    public int numberOfMessage;
}
