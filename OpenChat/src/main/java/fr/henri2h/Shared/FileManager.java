package fr.henri2h.Shared;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by henri on 27/11/16.
 */
public class FileManager {
    public static final Path main = Paths.get("").normalize();

    public static String[] listFiles(String[] directory) {
        Path file = getPath(directory);
        File f = new File(file.toString());
        return f.list();

    }

    public static List<String> loadFile(String[] directory, String FileName) throws IOException {
        Path file = getPath(directory, FileName);
        if (Files.exists(file)) {
            return Files.readAllLines(file, Charset.defaultCharset());
        } else {
            App.logProgram("The file [" + file.toString() + "] didn't exist");
            return null;
        }

    }

    public static void appendTextWithNewLine(String[] directory, String FileName, String content) throws IOException {
        Path file = getPath(directory, FileName, true);
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.toFile(), true), "UTF-8"));
        writer.append(content);
        // new line
        writer.append(System.lineSeparator());
        writer.close();
    }
    public static void appendText(String[] directory, String FileName, String content) throws IOException {
        Path file = getPath(directory, FileName, true);
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.toFile(), true), "UTF-8"));
        writer.append(content);
        writer.close();
    }

    public static void writeText(String[] directory, String FileName, String[] content) throws IOException {
        Path path = getPath(directory, FileName, true, true);
        writeText(path.toFile(), content);
    }

    public static File getFile(String[] directory, String filename) {
        return getPath(directory, filename).normalize().toFile();
    }

    public static File getFile(String[] directory, String filename, boolean createDirectory) {
        return getPath(directory, filename, createDirectory).normalize().toFile();
    }

    public static boolean exist(String[] directory, String filename) {
        return Files.exists(getPath(directory, filename));
    }

    public static boolean exist(String[] directory) {
        return Files.exists(getPath(directory));
    }

    public static Path getPath(String[] directory) {
        return getPath(directory, null, false);
    }

    public static Path getPath(String[] directory, boolean createDir) {
        return getPath(directory, null, createDir);
    }

    public static Path getPath(String[] directory, String filename) {
        return getPath(directory, filename, false);
    }

    public static Path getPath(String[] directory, String filename, boolean createDirectory) {
        return getPath(directory, filename, createDirectory, false);
    }

    public static Path getPath(String[] directory, String filename, boolean createDirectory, boolean createFile) {
        Path file = main;
        if (directory != null) {
            for (String Directory : directory) {
                file = file.resolve(Directory);
            }
        }

        // directories
        if (createDirectory) {
            // create the directories if not exist
            if (!Files.exists(file)) {
                try {
                    Files.createDirectories(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        // file
        if (filename != null) {
            file = file.resolve(filename);
        }
        
        if (createFile) {
            // create the directories if not exist
            if (!Files.exists(file)) {
                try {
                    Files.createFile(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        file = file.normalize();
        return file;
    }

    public static void writeText(File file, String[] content) throws IOException {
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), "UTF-8"));
        for (String line : content) {
            writer.append(line + System.lineSeparator());
        }
        writer.close();
    }


    public static void setFileContent(String fileName, String content) throws IOException {
        File file = getFile(null, fileName, true); // in  the root directory
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), "UTF-8"));
        writer.append(content);    
        writer.close();
    }
}


