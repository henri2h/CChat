====================================================

                Documentation

        Made by      : Henri2h
        Last updated : 21/10/2016
        Version      : 1.0.0

====================================================

This documentation is how the project must be. It is not how it work today !!!!

This is a small documentation to explain and setup how it work.
This project is divided in two part :
	The server : host all the conversations and the user database, command line
	The client : allow the user to connect to the server and send message. Must have a nice user interface


==============================================
If the command is a success => @OK else @KO
==============================================


============================= connection =====================================

When we connect, the client innitalize the connection to the server.
Once this is done, the server send "@OK" to say that, know he is able to respond to the client request


When the user want to connect, he add to ask the authorization to the server <client only>:

Client: "@connection"
Server :
    - @connectionOk
    - @connectionKO

get connection method <client only>:
=====================================
Client send : "@connectionMethod"
Sever : @<connectionMethods>

    connectionMethods :
        - anonymous
        - password (with username and password)


connection <client only>: 
=========================
Client: "@connect"
Server:
    - "@connectOK"
    - "@connectKO"

The connection procesus is started, the client must send his username and password

Client: <username>
Client: <password>
Server:  - "@connectSuccess" -> continue
         - "@connectFailed"  -> error, user not found

The client should now send his username and password in two different string line
If the connection is anonymous, the username is the name wich is going to be used and the password should be blank
You must !!! send the two string
//TODO : this could be a security issue, correct this
//TODO : add a better security when sending the password, maybe create a hash


Client send : "@connected"
Server respond "@connectedOK" if the client is logged and can send message or "@connectedKO" if he still need to logged himself

=================================== sending message ======================================================
The the client send the username and the password if needed

Sending message <server or client>:
the client must say that he is going to send a message
Client: "@message"
Server:    - "@OK" -> continue
           - "@KO" -> abort
Client: <size of the message in string : (length of the string)>
Client: <json serialised message (in string)>
The message is a json message comporting :
- client username
- utc date when the message has been send
- text message

exemple : {
"username":"henri"
"date":"<date>"
"message":"hello world"
}

sending short server message (single line message) <server or client>:
machine 1 : "@sMessage"
machine 2 : "@OK" -> continue or "@KO" -> abort
machine 1: "<message>"

===================== users ====================
//TODO : allow the client to download and upload some user data like downloading the list of users in a conversation and allow the user to chnage his personnal information
Get an user <client only> :
client send : "@users"
server send : "@OK" -> continue or "@KO" -> abort
client send : "@getUser"
client send : "<username>"
server send : "@OK" -> continue (user exist) ot "@KO" -> abort (not found)
server send : user information (json, see information bellow)

==================
user information :
==================
The client information are send in json form
example : {
    "username":"henri"
    "name":"Henri"
    "version":"1"
}

All the client must have an image <client only> :
=================================================
Client: "@users"
Server:  - "@OK" -> continue
         - "@KO" -> abort
Client: "@getUserImage"
Client: "username"
server: "@OK" -> continue (image exist and is readable) ot "@KO" -> abort (not found or something append)
Server: image size
Server: <image binary>

=================== disconection ===============
to close :
Client: "@disconnect"
Server: "@OKDisconenct" if ok or "@KODisconnect"

