# CChat :

*Work in progress*

This is a program to allow people to communicate over internet using a HTTP Rest API

This project is divided in two sides :

## Xamarin App
[![Build status](https://build.appcenter.ms/v0.1/apps/c11776db-71c7-4871-b5fc-668978c57f8f/branches/master/badge)](https://appcenter.ms)

A cross platfom app which run on Android, Windows Phones and ios

## Experimental : Ubuntu touch app :

Web app for the ubuntu touch platform.
Could be used on desktop or on other platform also.

## JAVA Server :
![Build status](https://github.com/henri2h/cchat/workflows/Java%20CI/badge.svg)
We are currently redisigning the client to have a rest server which will be more stable than a tcp connection

### How to build :
To build the jar file from the command line in /OpenChat
> mvn package

## Universal version

The universal version (only for windows phone, for desktop, use the xamarin one) is outdated, the priority is the xamarin version


## Screenshots

More to come ;)
